import zmq
import threading
import signal
import json
import converter
import logging
from dotenv import load_dotenv
import os

load_dotenv('.env.local')
load_dotenv('.env')

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

zmq_convert = os.getenv("ZMQ_CONVERT")
zmq_result = os.getenv("ZMQ_RESULT")

context = zmq.Context()

logger.info('Bind pull to {}'.format(zmq_convert))
req = context.socket(zmq.PULL)
req.bind(zmq_convert)

logger.info('Bind push to {}'.format(zmq_result))
res = context.socket(zmq.PUSH)
res.bind(zmq_result)

class ServiceExit(Exception):
    """
    Custom exception which is used to trigger the clean exit
    of all running threads and the main program.
    """
    pass

def service_shutdown(signum, frame):
    logger.info('Caught signal %d' % signum)
    raise ServiceExit

def process_message(message):
    data = json.loads(message)
    body = json.loads(data['body'])

    logger.info('Start task {}'.format(data['id']))
    logger.info('Convert from {} to {}'.format(body['inFile'], body['outFile']))

    converter.run(body['inFile'], body['outFile'], body['blur'], body['mute'])

    res.send(message)
    logger.info('End task {}'.format(data['id']))

def main():
    signal.signal(signal.SIGTERM, service_shutdown)
    signal.signal(signal.SIGINT, service_shutdown)
    try:
        while True:
            message = req.recv()
            threading.Thread(target=process_message, args=(message,)).start()
    except ServiceExit:
        main_thread = threading.current_thread()
        for thread in  threading.enumerate():
            if thread is main_thread:
                continue
            logger.info('Waiting for thread {}'.format(thread.name))
            thread.join()
    logger.info('Exit')

if __name__ == '__main__':
    main()
