import cv2
import face_detection
import logging
import ffmpeg
import numpy as np

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

maxrate = '3M'
scale_width = 'min(1280,iw)'
scale_height = 'min(1280,ih)'

detector = face_detection.build_detector("RetinaNetMobileNetV1", confidence_threshold=.5, nms_iou_threshold=.3)

def get_video_size(filename):
    logger.info('Getting video size for {!r}'.format(filename))
    probe = ffmpeg.probe(filename)
    video_info = next(s for s in probe['streams'] if s['codec_type'] == 'video')
    width = int(video_info['width'])
    height = int(video_info['height'])
    return width, height

def start_ffmpeg_process1(in_filename):
    logger.info('Starting ffmpeg process1')
    process1 = (
        ffmpeg
        .input(in_filename)
        .output('pipe:', format='rawvideo', pix_fmt='rgb24')
        .global_args('-loglevel', 'error')
        .run_async(pipe_stdout=True)
    )
    return process1

def start_ffmpeg_process2(out_filename, width, height, audio):
    logger.info('Starting ffmpeg process2')
    stream = ffmpeg.input('pipe:', format='rawvideo', pix_fmt='rgb24', s='{}x{}'.format(width, height))
    stream = ffmpeg.filter(stream, "scale", w=scale_width, h=scale_height, force_original_aspect_ratio='decrease')
    if (audio):
        stream = ffmpeg.output(stream, audio, out_filename, pix_fmt='yuv420p', maxrate=maxrate)
    else:
        stream = ffmpeg.output(stream, out_filename, pix_fmt='yuv420p', maxrate=maxrate)
    stream = ffmpeg.overwrite_output(stream).global_args('-loglevel', 'error')

    return ffmpeg.run_async(stream, pipe_stdin=True)

def blur_faces(frame):
    faces = detector.detect(frame[:, :, ::-1])
    img = np.array(frame)
    logger.debug('Detect {!r} faces'.format(len(faces)))
    for face in faces:
        (x, y, x2, y2, score) = face
        x = int(x)
        y = int(y)
        x2 = int(x2)
        y2 = int(y2)
        img[y:y2, x:x2] = cv2.GaussianBlur(frame[y:y2, x:x2], (101, 101), 30)
    return img

def do_nothing(frame):
    return frame

def read_frame(process1, width, height):
    logger.debug('Reading frame')

    # Note: RGB24 == 3 bytes per pixel.
    frame_size = width * height * 3
    in_bytes = process1.stdout.read(frame_size)
    if len(in_bytes) == 0:
        frame = None
    else:
        assert len(in_bytes) == frame_size
        frame = (
            np
            .frombuffer(in_bytes, np.uint8)
            .reshape([height, width, 3])
        )
    return frame

def write_frame(process2, frame):
    logger.debug('Writing frame')
    process2.stdin.write(
        frame
        .astype(np.uint8)
        .tobytes()
    )

# https://github.com/kkroening/ffmpeg-python/issues/204#issuecomment-593448324
def has_audio_streams(file_path):
    streams = ffmpeg.probe(file_path)["streams"]
    for stream in streams:
        if stream["codec_type"] == "audio":
            return True
    return False

def run(in_filename, out_filename, blur = False, mute = False):
    width, height = get_video_size(in_filename)
    process1 = start_ffmpeg_process1(in_filename)

    if ((not mute) and has_audio_streams(in_filename)):
        audio = ffmpeg.input(in_filename).audio
    else:
        audio = None

    if (blur):
        process_frame = blur_faces
    else:
        process_frame = do_nothing

    process2 = start_ffmpeg_process2(out_filename, width, height, audio)
    while True:
        in_frame = read_frame(process1, width, height)
        if in_frame is None:
            logger.info('End of input stream')
            break

        logger.debug('Processing frame')
        out_frame = process_frame(in_frame)
        write_frame(process2, out_frame)

    logger.info('Waiting for ffmpeg process1')
    process1.wait()

    logger.info('Waiting for ffmpeg process2')
    process2.stdin.close()
    process2.wait()

    logger.info('Done')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Convert video')
    parser.add_argument('in_filename', help='Input filename')
    parser.add_argument('out_filename', help='Output filename')
    parser.add_argument('--blur', action='store_true', help='Blur faces')
    parser.add_argument('--mute', action='store_true', help='Mute', default=False)

    args = parser.parse_args()
    run(args.in_filename, args.out_filename, args.blur, args.mute)