help:
	@echo "Hi"

.venv:
	python3 -m venv .venv

install: .venv
	.venv/bin/pip install -r requirements.txt

run:
	.venv/bin/python server.py